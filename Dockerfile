FROM php:5.6-cli

MAINTAINER <julien.fastre@champs-libres.coop>

RUN apt-get update && apt-get -y install wget ca-certificates git \
   && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add -

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list

RUN apt-get update && apt-get -y install libicu52 \ 
   libicu-dev \
   g++ \
   postgresql-server-dev-9.4 \
 && docker-php-ext-install intl pdo_pgsql mbstring zip \
 && apt-get remove -y wget libicu-dev \
   g++ \
   && apt-get autoremove -y 

#php configuration
RUN echo "\
[Date] \
date.timezone = Europe/Brussels" \
   >> /usr/local/etc/php/php.ini

#install composer
WORKDIR /tmp
ADD https://getcomposer.org/installer installer
RUN cat installer | php \
   && mv composer.phar /usr/local/bin/composer \
   && rm /tmp/installer

#install phpunit
ADD https://phar.phpunit.de/phpunit.phar /usr/bin/phpunit
RUN chmod u+x /usr/bin/phpunit

ENV COMPOSER_HOME=/var/composer
VOLUME /var/composer

CMD ["/bin/bash"]

WORKDIR /app

